@extends('master')

@section('title')
Buat Akun Baru
@endsection

@section('content')
<form action="/welcome" method="POST">
    @csrf

    <label>First name: </label><br>
    <input type="text" name="Fname"><br>

    <label>Last name:</label><br>
    <input type="text" name="Lname"><br>

    <label>Gender:</label><br>
      <input type="radio" id="Male" name="Gender" value="Male">
      <label for="Male">Male</label><br>
      <input type="radio" id="Female" name="Gender" value="Female">
      <label for="Female">Female</label><br>
      <input type="radio" id="other" name="Gender" value="other">
      <label for="other">other</label>

    <label>Nationality:</label><br>
    <select name="Nationality">
        <option value="Indoneisa">Indoneisa</option>
        <option value="Jepang">Jepang</option>
        <option value="Korea">Korea</option>
    </select><br>

    <label>Language Spoken:</label><br>
    <input type="checkbox">Indoneisa <br>
    <input type="checkbox">English <br>
    <input type="checkbox">otner <br>

    <label>Bio:</label><br>
    <textarea name="Bio" id="bio" cols="30" rows="10"></textarea> <br>

    <input type = "submit" value = "Kirim">
</form>
@endsection
