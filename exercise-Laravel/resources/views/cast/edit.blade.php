@extends('master')
@section('title')
Edit CAST
@endsection

@section('content')
<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('put')
    <div class="mb-3">
      <label  class="form-label">Name : </label>
      <input name = "nama" type="text" class="form-control" value="{{old('nama',$cast->nama)}}"> <!-- old di gunakan agar file yang salah tidak terhapus-->
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="mb-3">
      <label  class="form-label">Age : </label>
      <input name = "umur" type="umur" class="form-control" value="{{old('nama',$cast->umur)}}" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="mb-3">
        <label class="form-label">Bio : </label>
        <textarea name="bio" id="form-control" cols="30" rows="5">{{old('nama',$cast->bio)}}</textarea>
      </div>
      @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection