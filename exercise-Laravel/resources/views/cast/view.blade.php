@extends('master')
@section('title')
CAST
@endsection
@push('script')
<script src="{{ asset('template/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Add<a/>
    <table class="table" id="example1">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Age</th>
            <th scope="col">Detail</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $items)
            <tr>
                <th scope="row">{{ $key +1 }}</th>
                <td>{{ $items->nama }}</td>
                <td>{{ $items->umur }}</td>
                <td>
                    <form action="/cast/{{ $items->id }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{ $items->id }}" class="btn btn-info btn-sm">Detail<a/>
                        <a href="/cast/{{ $items->id }}/edit" class="btn btn-warning btn-sm">Edit<a/>
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
                    </tr>
            @empty
                <h1>Empty Data</h1>
            @endforelse
         
        </tbody>
      </table>
@endsection