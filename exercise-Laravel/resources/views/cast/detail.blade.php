@extends('master')
@section('title')
Detai CAST
@endsection

@section('content')
<h1 class="text-primary">{{ $cast->nama }}</h1>
<p>{{ $cast -> bio }}</p>

<a href="/cast" class="btn btn-secondary my-2">Back</a>
@endsection