@extends('master')
@section('title')
Add CAST
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="mb-3">
      <label  class="form-label">Name : </label>
      <input name = "nama" type="text" class="form-control" value="{{old('nama')}}"> <!-- old di gunakan agar file yang salah tidak terhapus-->
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="mb-3">
      <label  class="form-label">Age : </label>
      <input name = "umur" type="umur" class="form-control" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="mb-3">
        <label class="form-label">Bio : </label>
        <textarea name="bio" id="form-control" cols="30" rows="5"></textarea>
      </div>
      @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection