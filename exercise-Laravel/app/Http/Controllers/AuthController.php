<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Registrasi(){
        return view('registrasi');
    }
    public function Welcome(Request $request){
        $FullName = $request['Fname']. " ".$request['Lname'];

        return view('welcome',['FullName'=> $FullName]);
    }
}
