<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\Node\Expr\FuncCall;
use Illuminate\Support\Facades\DB;

class Castcontroller extends Controller
{
    public function Create(){
        return view('cast.create');
    }
    public function DBC_Cast(Request $request){
        $request->validate([
            'nama'=>'required|min:2',
            'umur'=>'required',
            'bio'=>'required',
        ],
    [
        'nama.required' => "fill in the name at least 2 letters",
    ]);
    DB::table('cast')->insert([
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio'],
    ]);
    return redirect('/cast');

    }
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.view', ['cast' => $cast]);
    }
    public function Show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail',['cast'=>$cast]);
    }
    public function Edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit',['cast'=>$cast]);
    }

    public function Update(Request $request,$id){
        $request->validate([
            'nama'=>'required|min:2',
            'umur'=>'required',
            'bio'=>'required',
        ],
    [
        'nama.required' => "fill in the name at least 2 letters",
    ]);
    DB::table('cast')
    ->where('id',$id)
    ->update(
        [
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]
        );
        return redirect('/cast');
    }

    public function Destory ($id){
        DB::table('cast')->where('id','=',$id)->delete();
        return redirect('/cast');
    }
}
