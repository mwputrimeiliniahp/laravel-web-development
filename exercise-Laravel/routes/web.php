<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Castcontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get ('/',[HomeController::class,'Home']);
Route::get ('/registrasi',[AuthController::class,'Registrasi']);
Route::post ('/welcome',[AuthController::class,'Welcome']);

Route::get('/data-table',function(){
    return view('data-table');
});

Route::get('/table',function(){
    return view('table');
});

//CRUD Cast
//Create
//Route untuk mengarah ke form tambah cast
Route::get('/cast/create',[Castcontroller::class,'Create']);

//Root untuk menyimpan data inputan ke db 
Route::post('/cast',[Castcontroller::class,'DBC_Cast']);

//Read
// Menampilkan semua daya yang ada di db 
Route::get('/cast',[Castcontroller::class,'index']);
//detail kategori berdasarkan ID 
Route::get('/cast/{cast_id}',[Castcontroller::class,'Show']);

//Update
//Route untuk mengarah ke form update berdasrkan ID
Route::get('/cast/{cast_id}/edit',[Castcontroller::class,'Edit']);
//untuk mengedit data berdasarkan ID di db 
Route::put('/cast/{cast_id}',[Castcontroller::class,'Update']);

//Detele
Route::delete('/cast/{cast_id}',[Castcontroller::class,'Destory']);