@extends('layouts.master')
@section('title')
Create Gender
@endsection

@section('content')
<form action="/genre" method="POST">
    @csrf
    <div class="mb-3">
      <label  class="form-label">Name : </label>
      <input name = "name" type="text" class="form-control" value="{{old('name')}}"> <!-- old di gunakan agar file yang salah tidak terhapus-->
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection