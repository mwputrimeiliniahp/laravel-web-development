@extends('layouts.master')
@section('title')
Create Books
@endsection

@section('content')
<form action="/books" method="POST">
    @csrf
    <div class="mb-3">
      <label  class="form-label">Title : </label>
      <input name = "title" type="text" class="form-control" value="{{old('title')}}"> <!-- old di gunakan agar file yang salah tidak terhapus-->
    </div>
    @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="mb-3">
    <label class="form-label">Description : </label>
    <textarea name="description" id="form-control" cols="100" rows="5"></textarea>
  </div>
  @error('description')
<div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="mb-3">
      <label  class="form-label">Author : </label>
      <input name = "author" type="author" class="form-control" >
    </div>
    @error('author')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="mb-3">
    <label  class="form-label">Years : </label>
    <input name = "years" type="years" class="form-control" >
  </div>
  @error('years')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
  <label >genre</label>
  <select name="genre_id" class="form-control" id="">
    <option value="">---select genre---</option>
    @forelse ($genre as $item)
        <option value="{{old('genre_id')}}">{{$message}}</option>
    @empty
        
    @endforelse
  </select>

</div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection