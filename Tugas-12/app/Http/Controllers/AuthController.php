<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view('Register');
    }
    public function Welcome(Request $request){
        $FullName = $request['Fname']. " ".$request['Lname'];

        return view('welcome',['FullName'=> $FullName]);
    }
}
